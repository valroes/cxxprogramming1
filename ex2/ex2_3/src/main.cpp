#include <iostream>
#include <random>
#include <cmath>

double calcPi(const int& iterations);
bool isInCircle(const double& x, const double& y);

int main() {
    double result = calcPi(5000000);
    std::cout << "Calulated pi. Result: " << result << std::endl;

    return 0;
}

double calcPi(const int& iterations) {
    //Initialize random number generator
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> dist(0.0, 1.0);

    double x,y;
    int nmbInCircle{0}; 
    for (int i=0; i<iterations; ++i) {
        //Throw a dart
        x =  dist(mt);
        y =  dist(mt);

        //Check if the dart landet in the circle
        if (isInCircle(x, y)){nmbInCircle++;};
    }

    //Calculate Pi
    return (nmbInCircle / (float)iterations)*4;
}

bool isInCircle(const double& x, const double& y) {
    //Calculate eclidean distance and check if its smaller then one
    return ( sqrt(pow(x, 2) + pow(y, 2)) < 1 );
}
