#ifndef CALCULATOR_H
#define CALCULATOR_H
#include <string>

int addReference(const int& nmb1, const int& nmb2);

void addPassBy(const int& nmb1, const int &nmb2, int& result);

int addNumbers(const int& nmb1=0,
               const int& nmb2=0,
               const int& nmb3=0,
               const int& nmb4=0
               );

void printType(int intNumber);
void printType(double doubleNumber);
void printType(float floatNumber);
void printType(std::string strWord);

#endif //CALCULATOR_H
