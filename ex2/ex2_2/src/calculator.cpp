#include <iostream>
#include <string>

int addReference(const int& nmb1, const int& nmb2){
    return nmb1 + nmb2;
}

void addPassBy(const int& nmb1, const int &nmb2, int& result){
    result = nmb1 + nmb2;
}

int addNumbers(const int& nmb1=0,
               const int& nmb2=0,
               const int& nmb3=0,
               const int& nmb4=0
               ) {
    return nmb1 + nmb2 + nmb3 + nmb4;
}

void printType(int intNumber){
    std::cout << "integer" << std::endl;
}
void printType(double doubleNumber){
    std::cout << "double" << std::endl;
}
void printType(float floatNumber){
    std::cout << "float" << std::endl;
}
void printType(std::string strWord){
    std::cout << "string" << std::endl;
}
