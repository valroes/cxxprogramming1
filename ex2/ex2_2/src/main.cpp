#include <iostream>
#include <string>
#include "calculator.h"

/* --Answers to Questions------------------------------------------------------------
 * 2.1.3 The arg num is not given by copy. Therefor the number in the scope of the
 * main function, is not changed by the function. To make it work declare the
 * function like this:
 * void doubleNumber(int& num){num = num * 2;}
 */

int main() {
    int nmb1, nmb2, result;

    //2.1.1
    std::cout << "Give me two numbers to add\n: ";
    std::cin >> nmb1;
    std::cout << "An other one\n: ";
    std::cin >> nmb2;
    std::cout << "The result is: " << addReference(nmb1, nmb2) << std::endl;

    //2.1.2
    addPassBy(nmb1, nmb2, result);
    std::cout << "Pass by reference result:  " << result <<std::endl;

    //2.2
    result = 0;
    result = addNumbers(1,2,3,4);
    std::cout << "Result 1: " << result << std::endl;
    result = addNumbers(1,2,3);
    std::cout << "Result 2: " << result << std::endl;
    result = addNumbers(1,2);
    std::cout << "Result 3: " << result << std::endl;
    result = addNumbers(1);
    std::cout << "Result 4: " << result << std::endl;
    result = addNumbers();
    std::cout << "Result 5: " << result << std::endl;

    //2.3
    int intNumber = 42;
    printType(intNumber);

    float floatNumber = 1.666;
    printType(floatNumber);

    double doubleNumber = 3.14;
    printType(doubleNumber);

    std::string strWord = "Hello World!";
    printType(strWord);




    return 0;
}
