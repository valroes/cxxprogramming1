#include <iostream>
#include "vector.h"

void copy(int* oldArr, int* newArr, const int& size) {
    for (int i=0; i < size; i++) {
        newArr[i] = oldArr[i];
    }
}

Vector::Vector() {
    m_data = new int[0];
    m_size = 0;
}

Vector::Vector(int size)
    : m_size(size)
{
    m_data = new int[m_size];
    initArray(0);
}

Vector::Vector(int size, int value)
    :m_size(size)
{
    m_data = new int[m_size];
    initArray(value);
}

Vector::~Vector() {
    delete [] m_data;
}

//Copy Constructor
Vector::Vector(const Vector& copySource) {
    std::cout << "copy cTor called" << std::endl;
    m_data = nullptr;
    m_size = copySource.size();
    
    if (copySource.m_data != nullptr) {
        m_data = new int[copySource.size()];
        copy(copySource.m_data, m_data, copySource.size());
    }
}

//Copy Assignment
Vector& Vector::operator=(const Vector& copySource) {
    std::cout << "copy assigment called" << std::endl;
    if ( (this != &copySource) and (copySource.m_data != nullptr) ) {
        delete[] m_data;
        m_data = new int[copySource.size()];
        for (int i=0; i<copySource.size(); i++) {
            m_data[i] = copySource.m_data[i];
        }
        m_size = copySource.m_size;
    }
    return *this;
}

//Move Constructor
Vector::Vector(Vector&& moveSource)
    : m_data(moveSource.m_data), m_size(moveSource.m_size) {
        std::cout << "move cTor called" << std::endl;
        moveSource.m_data = nullptr;
        moveSource.m_size = 0;
    }

//Move Assignment
Vector& Vector::operator=(Vector&& moveSource) {
    std::cout << "move assignment called" << std::endl;
    if ( (this != &moveSource) and (moveSource.m_data != nullptr) ) {
        delete[] m_data;
        m_data = moveSource.m_data;
        m_size = moveSource.size();
        moveSource.m_data = nullptr;
    }
    return *this;
}


//+
Vector Vector::operator+(Vector& other) {
    //Should check if sizes are equal and throw otherwise
    Vector newVector;

    newVector.m_size = size();
    newVector.m_data = new int[size()];
    for (int i=0; i<size(); i++) {
        newVector.m_data[i] = at(i) + other.at(i);
    }
    return newVector;
}

Vector Vector::operator*(Vector& other) {
    Vector newVector;
    newVector.m_size = size();
    newVector.m_data = new int[size()];
    for (int i=0; i<size(); i++) {
        newVector.m_data[i] = at(i) * other.at(i);
    }
    return newVector;
}


std::ostream& operator<<(std::ostream& os, const Vector& vec) {
    int nmbToPrint{5};
    if (vec.size() < 5)
        nmbToPrint = vec.size();

    os << "[";
    for (int i=0; i<nmbToPrint; i++) {
        os << vec.at(i);
        if (i < nmbToPrint -1)
            os << ", ";
    }
    os << "]";
    return os;
}

void Vector::initArray(int value)
{
    for (int i=0; i < this->size(); i++)
        *(this->m_data + i) = value;
}

int Vector::size() const {return m_size;}

int Vector::at(int index) const {
    if (index >= m_size) {
        std::cout << "WARNING Index out of range." << std::endl;
        //throw std::out_of_range {"Vector::at()"};
        return 0;
    } else {
        return m_data[index];
    }
}

int Vector::operator[](int index) {
    return Vector::at(index);
}

void Vector::push_back(int value) {
    int* new_data = new int[size()+1];
    new_data[size()] = value;

    copy(m_data, new_data, size());
    delete [] m_data;
    m_size++;

    m_data = new_data;
}

void Vector::pop_back() {
    int* new_data = new int[size()-1];

    copy(m_data, new_data, size()-1);
    delete [] m_data;
    m_size--;

    m_data = new_data;
}

void Vector::clear() {
    delete [] m_data;
    m_data = new int[0];
    m_size = 0;
}

void pop_back() {
}

void clear() {}
