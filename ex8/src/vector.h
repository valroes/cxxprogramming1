#ifndef LIBRARY_H
#define LIBRARY_H
#include <iostream>

void copy(int* oldArr, int* newArr, const int& size);

class Vector {
public:
    Vector();
    Vector(int size);
    Vector(int size, int value);
    ~Vector();
    Vector(const Vector& copySource);
    Vector(Vector&& moveSource);
    Vector& operator=(const Vector& copySource);
    Vector& operator=(Vector&& movesource);
    Vector operator+(Vector& other);
    Vector operator*(Vector& other);

    int size() const;
    int at(int index) const;
    int operator[](int index);
    void push_back(int value);
    void pop_back();
    void clear();

    friend std::ostream& operator<<(std::ostream& os, const Vector& vec);

private:
    int* m_data;
    int m_size;

    void initArray(int value);
};

#endif //LIBRARY_H
