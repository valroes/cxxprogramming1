#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <numeric>

void numberSummaryArrays();
int sumA(int* arr, const int& n);
int sumB(int* arr, const int& n);
int minA(int* arr, const int& n);
int minB(int* arr, const int& n);
int maxA(int* arr, const int& n);
int maxB(int* arr, const int& n);
void numberSummarySmartPtr();
void numberSummaryVector();

int main() {
    //numberSummaryArrays();
    //numberSummarySmartPtr();
    numberSummaryVector();

    return 0;
}
 
//2.1.3 Implement the program with vectors
//----------------------------------------------------------------
void numberSummaryVector() {
    int nmbValues{0};
    std::cout << "How many values do you want to enter?: ";
    std::cin >> nmbValues;

    std::vector<int> numbers;
    int nmb {0};
    for (int i=0; i<nmbValues; i++) {
        std::cout << i + 1 << ".  value:  ";
        std::cin >> nmb;
        numbers.push_back(nmb);
    }
    std::cout << "---------------" << std::endl;

    int sum = std::accumulate(numbers.begin(), numbers.end(), 0);
    int min = *std::min_element(numbers.begin(), numbers.end());
    int max = *std::max_element(numbers.begin(), numbers.end());

    std::cout << "Results for a: " << std:: endl
              << "Sum:   "  << sum << std::endl
              << "Min:   "  << min << std::endl
              << "Max:   "  << max << std::endl;
}

//2.1.2 Implement the program with smart pointers
//----------------------------------------------------------------
void numberSummarySmartPtr() {
    int nmbValues{0};

    std::cout << "How many values do you want to enter?: ";
    std::cin >> nmbValues;

    std::unique_ptr<int[]> numbers{new int[nmbValues]};
    for (int i=0; i<nmbValues; i++) {
        std::cout << i + 1 << ".  value:  ";
        std::cin >> numbers[i];
    }
    std::cout << "---------------" << std::endl;

    int sum{0};
    int max{0};
    int min{numbers[0]};

    for (int i=0; i<nmbValues; i++) {
        sum += numbers[i];
        if (numbers[i] < min)
            min = numbers[i];
        if (numbers[i] > max)
            max = numbers[i];
    }

    std::cout << "Results for a: " << std:: endl
              << "Sum:   "  << sum << std::endl
              << "Min:   "  << min << std::endl
              << "Max:   "  << max << std::endl;
}

//2.1.1 Implement the program with unsafe arrays
//----------------------------------------------------------------
void numberSummaryArrays() {
    int nmbValues{0};

    std::cout << "How many values do you want to enter?: ";
    std::cin >> nmbValues;

    int* numbers = new int[nmbValues];
    for (int i=0; i<nmbValues; i++) {
        std::cout << i + 1 << ".  value:  ";
        std::cin >> *(numbers+i);
    }
    std::cout << "---------------" << std::endl;

    int sum1 = sumA(numbers, nmbValues);
    int sum2 = sumB(numbers, nmbValues);
    int min1 = minA(numbers, nmbValues);
    int min2 = minB(numbers, nmbValues);
    int max1 = maxA(numbers, nmbValues);
    int max2 = maxB(numbers, nmbValues);

    std::cout << "Results for a:  " << std:: endl
              << "Sum:   "  << sum1 << std::endl
              << "Min:   "  << min1 << std::endl
              << "Max:   "  << max1 << std::endl;
    std::cout << "Results for b:  " << std:: endl
              << "Sum:   "  << sum2 << std::endl
              << "Min:   "  << min2 << std::endl
              << "Max:   "  << max2 << std::endl;

    delete [] numbers;
}

//2.1.1.a Calculate results with index acces using the operator []
//----------------------------------------------------------------
int sumA(int* arr, const int& n) {
    int sum{0};
    for (int i=0; i<n; i++) {
        sum += arr[i];
    }
    return sum;
}

int minA(int* arr, const int& n) {
    int min = arr[0];
    for (int i=0; i<n; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min;
}

int maxA(int* arr, const int& n) {
    int max = 0;
    for (int i=0; i<n; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

//2.1.1.b Calculate results with pointer arithmetic using *
//---------------------------------------------------------------
int minB(int* arr, const int& n) {
    int min = *arr;
    for (int i=0; i<n; i++) {
        if ( *(arr + i) < min ) {
            min = *(arr + i);
        }
    }
    return min;
}


int sumB(int* arr, const int& n) {
    int sum{0};
    for (int i=0; i<n; i++) {
        sum += *(arr + i);
    }
    return sum;
}


int maxB(int* arr, const int& n) {
    int max = 0;
    for (int i=0; i<n; i++) {
        if ( *(arr + i) > max ) {
            max = *(arr + i);
        }
    }
    return max;
}

