#include <iostream>

int pitfall1();
int pitfall2();
int pitfall4();
int* allocateArray(const int length);

int main() {
    pitfall1();
    pitfall2();
    int* arr = allocateArray(5);
    std::cout << "Allocated array at: " << arr << std::endl;
    pitfall4();
}

int pitfall1() {
    //Pobably better readable if you put asterix directly after int
    int* pointToAnInt = new int;
    //pointToAnInt = 9;    <- pointToAnInt is of type pointer to int. Here it is
    //Attempted to write an int to it wich is not allowed. You have to dereference it
    //first.
    *pointToAnInt = 9;
    std::cout << "The value at pointToAnInt: " << *pointToAnInt;
    delete pointToAnInt;

    return 0;
}

//Both pointers point to the same value, when one is destroyed the data it is pointing
//to is also destroyed. So when the second pointer is destroyed you get a double free.
int pitfall2() {
    //Need to declare as pointer to int as this is what new returns.
    int* pointToAnInt = new int;
    int* pNumberCopy = pointToAnInt;
    *pNumberCopy = 30;
    std::cout << *pointToAnInt;
    //delete pNumberCopy;
    delete pointToAnInt;
    //delete pNumberCopy; This causes the crash

    return 0;
}

//The return type of the function was declared as pointer to int but an int was
//returned. We need to use new to allocate the memory for this array. This will
//return an pointer to first int in the array.
int* allocateArray(const int length) {
    int* temp = new int[length];
    return temp;
}

//The array only has 4 values in it. in the for loop it was iterratet over 6 times.
//At count=4 0 was printed, as this position of the array was allocated but not written
//to yet.
//At count=5 this was not part of the array so whatever value happened to be at that
//address was printet.
int pitfall4() {
    int array[5] {0, 1, 2, 3};
    for (int count = 0; count < 4; ++count)
        std::cout << array[count] << " ";

    return 0;
}
