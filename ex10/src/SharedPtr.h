#ifndef SHAREDPTR_H
#define SHAREDPTR_H

class Counter {
public:
    Counter(): m_count(0) {};

    void increment() {m_count++;};
    void decrement() {m_count--;};
    int get() {return m_count;};


private:
    int m_count {};
};


template<typename T>
class SharedPtr {
public:
    //Constructor
    SharedPtr(): m_ptr(nullptr) {
        m_counter = new Counter();
    };

    SharedPtr(T* ptr): m_ptr(ptr) {
        m_counter = new Counter();
        m_counter->increment();
    };

    //Deconstructor
    ~SharedPtr() {
        m_counter->decrement();
        if (m_counter->get()==0) {
            delete m_ptr;
            delete m_counter;
        }
    };

    //Copy
    SharedPtr(const SharedPtr& other) {
        m_ptr = other.m_ptr;
        m_counter = other.m_counter;
        m_counter->increment();
    };

    //Copy Assign
    SharedPtr& operator=(const SharedPtr& other) {
        m_ptr = other.m_ptr;
        m_counter = other.m_counter;
        m_counter->increment();
        return *this;
    };

    //Move
    SharedPtr(SharedPtr&& other): m_ptr(other.m_ptr) {other.m_ptr = nullptr;};

    //Move Assign
    SharedPtr& operator=(SharedPtr&& other) {
        delete m_ptr;
        m_ptr = other.m_ptr;
        other.m_ptr = nullptr;
        return *this;
    };

    //Operator Overloading
    T& operator*() {return *m_ptr;};
    T* operator->() const {return m_ptr;};

    //Methods
    int useCount() {return m_counter->get();};



private:
    T* m_ptr;
    Counter* m_counter;
};


#endif //SHAREDPTR_H
