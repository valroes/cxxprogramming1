#ifndef UNIQUEPTR_H
#define UNIQUEPTR_H

template<typename T>
class UniquePtr {
public:
    //Constructor
    UniquePtr(T* ptr): m_ptr(ptr) {};

    //Deconstructor
    ~UniquePtr() {
        delete m_ptr;
    };

    //Copy
    UniquePtr(const UniquePtr&) = delete;

    //Copy Assign
    UniquePtr& operator=(const UniquePtr&) = delete;

    //Move
    UniquePtr(UniquePtr&& other): m_ptr(other.m_ptr) {
        other.m_ptr = nullptr;
    };

    //Move Assign
    UniquePtr& operator=(UniquePtr&& other) {
        delete m_ptr;
        m_ptr = other.m_ptr;
        other.m_ptr = nullptr;
        return *this;
    };

    T& operator*() {return *m_ptr;};
    T* operator->() const {return m_ptr;};


private:
    T* m_ptr;
};


template<typename T>
class UniquePtr<T[]> {
public:
    //Constructor
    UniquePtr(T* ptr): m_ptr(ptr) {};

    //Deconstructor
    ~UniquePtr() {
        delete[] m_ptr;
    };

    //Copy
    UniquePtr(const UniquePtr&) = delete;

    //Copy Assign
    UniquePtr& operator=(const UniquePtr&) = delete;

    //Move
    UniquePtr(UniquePtr&& other): m_ptr(other.m_ptr) {
        other.m_ptr = nullptr;
    };

    //Move Assign
    UniquePtr& operator=(UniquePtr&& other) {
        delete[] m_ptr;
        m_ptr = other.m_ptr;
        other.m_ptr = nullptr;
        return *this;
    };

    T& operator*() {return *m_ptr;};
    T* operator->() const {return m_ptr;};


private:
    T* m_ptr;
};

#endif //UNIQUEPTR_H
