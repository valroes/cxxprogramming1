#include "UniquePtr.h"

template<typename T>
UniquePtr::UniquePtr(T* ptr): m_ptr(ptr) {}

UniquePtr::~UniquePtr() {
    delete m_ptr;
};
