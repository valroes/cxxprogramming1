#include <iostream>
#include <limits>
#include <cmath>
#include <iomanip>
#include "fibonacci.h"

/* --Answers to the questions----------------------------
 * - int overflow at iteration 46 (starting at 1)
 * - the upper limit on my system is 2147483647
 * - you could use a bigger datatype such as long int. Since fibonacci is mostly
 *   for positive numbers only (I think) you could also use unsigned datatypes.
 */

int main() {
    int f = 1;
    int fprev = 0;
    int generation {0}; //Which iteration
    float r; //Approximation for the golden ratio

    std::cout << "The upper int limit of your system is ";
    std::cout << std::numeric_limits<int>::max() << std::endl;

    do {
        generation++;
        std::cout << "n: " << generation << " - Fibonnaci number: ";
        std::cout << f << std::endl;
        int tmp = fibonacci(f, fprev);

        //Check if an int overflow happened
        //This would fail, if the increment happened to be the int range + 1 for
        //example. There is probably a better way to do this.
        if (tmp < f) {
            std::cout << "An integer overflow has occured at iteration ";
            std::cout << generation << std::endl;
            std::cout << "Overflow value " << tmp << std::endl;

            break;
        }

        fprev = f;
        f = tmp;

        // 3.4 Approximate golden ratio and compare to the real one
        r = approximateGoldenRatio(f, fprev);
        printGoldenRatio(r);

    } while(true);
    std::cout << std::endl;
}
