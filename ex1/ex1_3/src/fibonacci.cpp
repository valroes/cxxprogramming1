#include <iostream>
#include <cmath>
#include <iomanip>

//Is this something I can do or should constants be declared in the header?
float GOLDENRATIO {
    (1 + std::sqrt(5)) / 2
    
};

int fibonacci(int f, int fprev) {
    return f + fprev;
}

float approximateGoldenRatio(float f, float fprev) {
    return f / fprev;
}

float calcDeviation(float r) {
    return (GOLDENRATIO - r) / GOLDENRATIO * 100;
} 

void printGoldenRatio(float r) {
    std::cout << std::setprecision(17) << std::fixed << "Ratio:  " << r;
    std::cout << " - Dev[%]: " << calcDeviation(r) <<std::endl;
}
