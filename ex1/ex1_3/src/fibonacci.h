#ifndef FIBONACCI_H
#define FIBONACCI_H


int fibonacci(int f, int fprev);

float approximateGoldenRatio(float f, float fprev);

float calcDeviation(float r);

void printGoldenRatio(float r);


#endif //FIBONACCI_H

