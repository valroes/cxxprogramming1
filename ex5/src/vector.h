#ifndef LIBRARY_H
#define LIBRARY_H

void copy(int* oldArr, int* newArr, const int& size);

class Vector {
public:
    Vector();
    Vector(int size);
    Vector(int size, int value);
    ~Vector();
    Vector(const Vector& copySource);
    Vector(Vector&& moveSource);

    int size() const;
    int at(int index);
    int operator[](int index);
    void push_back(int value);
    void pop_back();
    void clear();

private:
    int* m_data;
    int m_size;

    void initArray(int value);
};

#endif //LIBRARY_H
