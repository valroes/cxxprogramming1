#ifndef MULTIPLE_INHERITANCE_H
#define MULTIPLE_INHERITANCE_H
#include <iostream>
namespace order {

class A {
public:
    A() {std::cout << "A()\n";};
    ~A() {std::cout << "~A()\n";};
};

class B {
public:
    B() {std::cout << "B()\n";};
    ~B() {std::cout << "~B()\n";};
};

class C {
public:
    C() {std::cout << "C()\n";};
    ~C() {std::cout << "~C()\n";};
};

class ACB: public A, public C, public B {
};

class BCA: public B, public C, public A {
};

}

#endif //MULTIPLE_INHERITANCE_H
