#include <iostream>
#include "checkingaccount.h"
#include "account.h"

CheckingAccount::CheckingAccount(float initialBalance, float fee)
    : Account(initialBalance), m_fee(fee) {}

void CheckingAccount::debit(float amount) {
        //if (amount + m_fee > m_balance)
        //   std::cout << "WARNING Not enough money. Refusing transaction." << std::endl;
        //else
        //   m_balance = m_balance - amount - m_fee;
        Account::debit(amount+m_fee);
        }
