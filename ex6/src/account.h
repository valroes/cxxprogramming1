#ifndef ACCOUNT_H
#define ACCOUNT_H

class Account {
public:
    Account(float initalBalance);

    void credit(float amount);
    virtual void debit(float amount);
    void printBalance();


private:
    float m_balance;
};

#endif //ACCOUNT_H
