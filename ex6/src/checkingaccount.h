#ifndef CHECKINGACCOUNT_H
#define CHECKINGACCOUNT_H
#include "account.h"

class CheckingAccount: public Account {
public:
    CheckingAccount(float initialBalance, float fee);
    void debit(float amount) override;

private:
    float m_fee;
};


#endif //CHECKINGACCOUNT_H
