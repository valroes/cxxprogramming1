#ifndef ACCESS_APECIFIERS_H
#define ACCESS_SPECIFIERS_H
#include <iostream>


namespace access {

// Variant 1: define m_A as protected member.
// Variant 2: define m_A as public.
// Variant 1 is prefered. Member variables should not be declared public. The variable
// can be accessed from outside the class and its derived classes which is not desired.
// Therefore declare as protected member so derived classes can still access but it's
// unacccesible from the outside.
class A {
protected:
    int m_A;
};

class B: private A {};

class C: public B {};

}

#endif //ACCESS_SPECIFIERS_H
