#include <iostream>
#include "checkingaccount.h"
#include "multiple_inheritance.h"


int main()
{
    CheckingAccount myAccount(1000, 0.5); // initial amount and fee
    myAccount.credit(250);    // ok
    myAccount.printBalance(); // -> 1250
    myAccount.debit(1000);    // ok
    myAccount.printBalance(); // -> 249.5
    myAccount.debit(249.5);   // Waring: Not enough funds
    myAccount.debit(249);     // ok
    myAccount.printBalance(); // -> 0

    /* Exercise 2.2
     * -----------------------------------------------------------------------------
     *  a) Output:
     *  Base()
     *  Derived()
     *  Base()
     *  ~Base()
     *  ~Derived()
     *  ~Base()
     *
     * b) The Derived class attempts to access m_x in the print function. m_x is
     * however a private member of the base class and can therefore not be accessed
     * by the derived class. The code won't compile.
     *
     * Exercise 2.3
     * -----------------------------------------------------------------------------
     * Ther order is first constructors A then C then B. Then destructors B then
     * C then A.
     * General rules:
     * 1. The constructors are called in the order they are specified in the class
     * definition from left to right.
     * 2. Desructors are called in reversed order, i.e the class whose constructor
     * was called last will have its destructor called first.
     *
     * Exercise 2.4 see file access_specifiers.h
     */


    //order::ACB abc = order::ACB();
    //order::BCA bca = order::BCA();


    return 0;
}
