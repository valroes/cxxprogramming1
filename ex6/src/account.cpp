#include <iostream>
#include "account.h"

Account::Account(float initialBalance)
    : m_balance(initialBalance) {}

void Account::credit(float amount) {
    m_balance += amount;
}

void Account::debit(float amount) {
    if (amount > m_balance)
        std::cout << "WARNING Not enough money. Refusing transaction." << std::endl;
    else
        m_balance -= amount;

}

void Account::printBalance() {
    std::cout << "Your balance is: " << m_balance << std::endl;
}
