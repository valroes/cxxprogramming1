#ifndef BOOK_H
#define BOOK_H
#include <string>

class Book {
public:
    Book(std::string title, std::string author, std::string isbn);
    bool getValid();
    void printInfo();

private:
    std::string m_title;
    std::string m_author;
    std::string m_isbn;
    bool m_isValid;

    void checkValidity();
    void checkEmptyStrings();
    void checkIsbn();

};

#endif //BOOK_H
