#include <iostream>
#include <string>
#include <algorithm>
#include "book.h"

Book::Book(std::string title, std::string author, std::string isbn) 
    :m_title(title), m_author(author), m_isbn(isbn) {
    checkValidity();
}

void Book::checkValidity() {
    m_isValid = true;
    checkEmptyStrings();
    checkIsbn();
}

void Book::checkEmptyStrings() {
    m_isValid = !(m_author.empty() or m_title.empty());
}

void Book::checkIsbn() {
    m_isbn.erase( std::remove(m_isbn.begin(), m_isbn.end(), '-'), m_isbn.end() );

    if (m_isbn.length() != 10) {
        m_isValid = false;
    } else {
        int sum{0};
        for (int i=0; i<10; ++i) {
            sum += std::stoi(m_isbn.substr(i,1)) * (i+1);
        }
        if (sum % 11 != 0)
            m_isValid = false;
    }
}

bool Book::getValid() {return m_isValid;}

void Book::printInfo(){
    std::cout << "Title: " << m_title << std::endl;
    std::cout << "Author: " << m_author << std::endl;
    std::cout << "ISBN: " << m_isbn << std::endl;
    std::cout << "Valid: " << m_isValid << std::endl;
}

