#ifndef LIBRARY_H
#define LIBRARY_H
#include <vector>
#include "book.h"

class Library {
public:
    void addBook(Book& book);
    void cleanup();
    void printInventory();

private:
    std::vector<Book> m_books;
    void deleteBook(int index);
};

#endif //LIBRARY_H
