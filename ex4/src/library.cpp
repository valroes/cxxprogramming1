#include <iostream>
#include <vector>
#include "library.h"

void Library::addBook(Book& book) {
    m_books.push_back(book);
}
void Library::cleanup() {
    std::vector<int> toDelete;
    for (std::vector<Book>::size_type i=0; i<m_books.size(); i++) {
        if (!m_books[i].getValid()) {
            toDelete.push_back(i);
        }
    }

    for (int& el : toDelete) {
        deleteBook(el);
    }
};
void Library::printInventory() {
    int nmbValid{0};
    for (auto& book : m_books)
        nmbValid += book.getValid();
    std::cout << m_books.size() << " in inventory. " << nmbValid <<
        " are valid " << m_books.size()-nmbValid << " are invalid." << std::endl;
}
void Library::deleteBook(int index) {
    std::cout << "Deleting book nmb" << index << std::endl;
    m_books.erase(m_books.begin()+(index));
};
