#include <string>
#include "stack.h"

int main() {
    Stack<int> stackInt;
    stackInt.push(1);
    stackInt.push(2);
    stackInt.push(3);
    stackInt.push(4);
    stackInt.print();
    std::cout << "pop: " << stackInt.pop() << std::endl;
    stackInt.print();
    std::cout << "pop: " << stackInt.pop() << std::endl;
    stackInt.print();
    std::cout << "pop: " << stackInt.pop() << std::endl;
    stackInt.print();
    std::cout << "peek: "<< stackInt.peek() << std::endl;
    stackInt.print();

    Stack<double> stackDouble;
    stackDouble.push(1.0);
    stackDouble.push(1.1);
    stackDouble.push(1.2);
    stackDouble.print();
    std::cout << "peek: " << stackDouble.peek() << std::endl;
    std::cout << "pop: " << stackDouble.pop() << std::endl;
    stackDouble.print();

    Stack<std::string> stackString;
    std::string s1 = "Hello";
    std::string s2 = "World";
    std::string s3 = "Foo";
    std::string s4 = "Bar";
    stackString.push(s1);
    stackString.push(s2);
    stackString.print();
    std::cout << stackString.pop() << std::endl;
    std::cout << stackString.peek() << std::endl;
    stackString.push(s3);
    stackString.push(s4);
    stackString.print();
    std::cout << "pop: " << stackString.pop() << std::endl;
    stackString.print();



    return 0;
}
