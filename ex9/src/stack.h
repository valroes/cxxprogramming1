#ifndef STACK_H
#define STACK_H
#include <array>
#include <iostream>


template<typename T>
class Stack {
public:
    void push(T el) {
        m_index++;
        m_data[m_index] = el;
        m_top = el;
    };

    T pop() {
        T temp = m_top;
        //I think if i do it like this I do not need to specialize for the
        //string case.
        m_data[m_index] = T{};
        m_index--;
        m_top = m_data[m_index];
        return temp;
    };

    T peek() {
        return m_top;
    };

    void print() {
        std::cout << "[";
        for (const T el: m_data) {
            std::cout << "'" << el << "' ";
        }
        std::cout << "]" << std::endl;
    };

private:
    std::array<T, 10> m_data {};
    T m_top;
    int m_index{-1};
};


#endif //STACK_H
