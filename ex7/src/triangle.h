#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "shape.h"

class Triangle final: public Shape {
public:
    Triangle(int width, int height);
    virtual ~Triangle();
    
    virtual float getArea() override;
    virtual float getCircumference() override;

private:
    int m_width;
    int m_height;

};

#endif //TRIANGLE_H
