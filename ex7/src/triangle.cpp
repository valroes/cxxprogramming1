#include <iostream>
#include "shape.h"
#include "triangle.h"
#include "math.h"

Triangle::Triangle(int width, int height): m_width(width), m_height(height) {
    m_type = "triangle";
}

Triangle::~Triangle() {
    std::cout << "Triangle destructor called" << std::endl;
}

float Triangle::getArea() {
    return (m_width * m_height) / 2;
}

float Triangle::getCircumference() {
    return m_width + std::sqrt( pow(m_width/2, 2) + m_height);
}

