#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.h"

class Circle final: public Shape {
public:
    Circle(int radius);
    virtual ~Circle();
    
    virtual float getArea() override;
    virtual float getCircumference() override;

private:
    int m_radius;

};

#endif //CIRCLE_H
