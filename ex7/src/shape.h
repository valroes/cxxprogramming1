#ifndef SHAPE_H
#define SHAPE_H
#include <string>

class Shape {
public:
    virtual ~Shape();
    virtual float getArea() = 0;
    virtual float getCircumference() = 0;
    void report();

    std::string m_type;
};

#endif //SHAPE_H
