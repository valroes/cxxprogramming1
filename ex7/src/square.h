#ifndef SQUARE_H
#define SQUARE_H

#include "shape.h"

class Square final: public Shape {
public:
    Square(int width);
    virtual ~Square();
    
    virtual float getArea() override;
    virtual float getCircumference() override;

private:
    int m_width;

};

#endif //SQUARE_H
