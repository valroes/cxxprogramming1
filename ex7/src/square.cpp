#include <iostream>
#include "shape.h"
#include "square.h"
#include "math.h"

Square::Square(int width): m_width(width) {
    m_type = "Square";
}

Square::~Square() {
    std::cout << "Square destructor called" << std::endl;
}

float Square::getArea() {
    return std::pow(m_width, 2);
}

float Square::getCircumference() {
    return m_width*4;
}
