#include <iostream> 
#include "shape.h"

Shape::~Shape() {
    std::cout << "Shape destructor called" << std::endl;
}

void Shape::report() {
    std::cout << m_type << " has an area: " << getArea()
              << " and circumference: " << getCircumference()
              << std::endl;
}

