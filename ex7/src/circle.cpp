#include <iostream>
#include "shape.h"
#include "circle.h"
#include "math.h"

Circle::Circle(int radius): m_radius(radius) {
    m_type = "circle";
}

Circle::~Circle() {
    std::cout << "Circle destructor called" << std::endl;
}

float Circle::getArea() {
    return M_PI * std::pow(m_radius, 2); 
}

float Circle::getCircumference() {
    return 2 * M_PI * m_radius;
}
